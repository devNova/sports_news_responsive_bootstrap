# Hockey News - Bootstrap Projekt mit Responsive Design

## Beschreibung

Dieses Übungsprojekt habe ich im Rahmen des ersten Semesters meiner Ausbildung zum Appplikationsentwickler umgesetzt. Es gehört zu einem meiner ersten Projekten im Bereich Webentwicklung. Das Hauptziel dieses Projekts bestand darin, ein besseres Verständnis für HTML, CSS und das Bootstrap-Framework zu entwickeln, insbesondere im Kontext des Responsive Designs. Am Ende des Projekts sollte eine funktionsfähige (statische) Website mit ansprechendem Responsive Design erstellt werden.

### Tech-Stack

- **HTML**

- **CSS**

- **Bootstrap** 